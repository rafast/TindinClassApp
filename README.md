Nome: Rafael Telles
Contato: (79) 9.9134-4520
E-mail: rafast.telles@gmail.com

Para o projeto eu utilizei o padrão MVC com  as ferramentas NodeJs, Express, MongoDB e Mongoose.

Não utilizei o Typescript versão 12 pois não tenho conhecimento em TS. Sendo assim, fiquei com receio de perder tempo tentando fazer o projeto em TS e não conseguir terminar a tempo. Havendo a possibilidade de extender o prazo, me comprometo a reescrever todo o código em TS, sem problemas.

Para autenticação usei o Passport com um plugin do Mongoose para fazer o cadastro do usuário, e uma vez logado, usei o JWT para criar um token e retornar para o usuário para as futuras requisições.

Criei um rota para adicionar um novo usuário afim de facilitar os testes da API.

Utilizei o middleware post do Mongoose para assim que uma aula for deletada, deletar também os seus respectivos comentários no banco.

Para colocar o projeto em produção eu utilizaria o EC2 da Amazon por ter baixo custo, alta disponibilidade e por eu já ter experiência prévia.





